package com.kshrd.spring_homework_001;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    ArrayList<Customer> customers = new ArrayList<>();

    public CustomerController() {
        customers.add(new Customer(1,"Lyheng","Male",22,"Phnom Phenh"));
        customers.add(new Customer(2,"Lyheang","Male",23,"Phnom Phenh"));
        customers.add(new Customer(3,"Mario","Male",12,"Phnom Phenh"));
    }
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer(){
        return customers;
    }
    @PostMapping("/customers")
    public Customer insertCustomer(@RequestBody Customer customer){
        customers.add(customer);
        return customer;
    }
@GetMapping ("/customers/{id}")
    public Customer getCustomerById(@PathVariable Integer id){
        for(Customer cus: customers){
            if(cus.getId() == id){
                return cus;
            }
        }
        return  null;
}
@GetMapping ("/customers/search")
public Customer findCustomerByName(@RequestParam String name){
        for(Customer cus: customers){
            if(cus.getName().equals(name)){
                return cus;
            }
        }
        return null;
}



        }
