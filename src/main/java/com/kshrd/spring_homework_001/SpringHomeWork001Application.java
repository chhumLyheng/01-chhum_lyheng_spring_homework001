package com.kshrd.spring_homework_001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHomeWork001Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringHomeWork001Application.class, args);
    }

}
